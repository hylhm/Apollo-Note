## Cyber Base  
base里提供了一些基础的类库：
1. atomic  hash map 
2. atomic  rw_lock  
3. bounded queue 
4. 并发对象池 
5. 线程池 
Cyber RT 中为了性能和效率，实现了一系列的Lock-Free的数据结构。
### AtomicHashMap 
基于hash桶实现的固定大小的hash表。
HashMap成员:
```cpp
Bucket table_[TableSize];
uint64_t capacity_;
uint64_t mode_num_;
```
Bucket为元素的链表：
Entry *head_; 
Bucket实现了Has、Find、Insert、Get等方法。
由于Entry的实现基于Atomic，所以能够实现AtomicHashMap。 
```cpp
  struct Entry {
    Entry() {}
    explicit Entry(K key) : key(key) {
      value_ptr.store(new V(), std::memory_order_release);
    }
    Entry(K key, const V &value) : key(key) {
      value_ptr.store(new V(value), std::memory_order_release);
    }
    Entry(K key, V &&value) : key(key) {
      value_ptr.store(new V(std::forward<V>(value)), std::memory_order_release);
    }
    ~Entry() { delete value_ptr.load(std::memory_order_acquire); }

    K key = 0;
    std::atomic<V *> value_ptr = {nullptr};
    std::atomic<Entry *> next = {nullptr};
  };
```
这里回顾下C++11 的原子量：
1. std::atomic_flag
atomic_flag是一种简单的原子bool类型，只支持test-and-set和clear操作，可以基于此实现简单的自旋锁。
2. std::atomic 
是一个模板类，用的时候先实例化。另外atomic还对整型和指针类型提供了特别的实现。
<br>
std::atomic提供如下的操作：
1. is_lock_free() 判断该原子量是否处于阻塞态
2. store() 修改原子量的值，同时可以指定内存序(不同线程的指令执行顺序，由于CPU和编译器都会对指令执行顺序做优化)
   一般情况store使用std::memory_order_release 模式，保证本线程中所有的写操作执行完成后才执行该原子操作。
   load使用memory_order_acquire模式保证所有的读操作在该原子指令之后执行。
3. load()读取原子量封装的值 

### AtomicRWLock 
该类实现了ReadLock，WriteLock，ReadUnlock，WriteUnlock给ReadLockGuard<AtomicRWLock>和WriteLockGuard<AtomicRWLock>使用。
<br>
ReadLockGuard<AtomicRWLock> 和 WriteLockGuard<AtomicRWLock>在构造的时候自动加锁，析构时候释放锁。

####  可重入锁 ReentrantRWLock 
同一线程可以多次持有锁，同时支持锁降级(同一线程在持有写锁的时候，可以获取读锁并释放)。

### WaitStrategy
提供了不同的线程同步策略：
1. BlockWaitStrategy  基于条件变量实现正常的Wait/Notify机制
2. SleepWaitStrategy  当前线程Sleep指定时间
3. YieldWaitStrategy  当前线程直接yeild，放弃当前执行的CPU时间片，由操作系统调度到其他线程执行
4. BusySpinWaitStrategy 直接返回true
5. TimeoutBlockWaitStrategy 设置超时的Wait


### placement new 
1. 在已经分配好的内存上直接构造对象，不需要分配内存，更加高效。 
2. 已经分配好的内存可以反复使用，可以有效避免内存碎片。
在new的时候传入内存地址，析构的时候只析构对象不释放内存。

### BoundedQueue
基于WaitStrategy和对象池实现的固定size的队列 
### UnboundedQueue
基于原子量和链表实现的队列。

### CCObjectPool 
基于原子量实现的并发对象池。

### signal
提供事件处理和回调 

### thread_safe_queue  
基于条件变量实现的线程安全队列。








