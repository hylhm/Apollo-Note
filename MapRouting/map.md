## Apollo Map 
Apollo的map模块包含了Hdmap、relative_map、pnc_map 
1. Hdmap包含Road、Lane、Junction等一系列的地图元素以及从文件中加载地图
2. pnc_map 负责读取和解析routing的搜索结果
3. relative_map模块基于HDmap、感知车道线等生成车身坐标系(FLU)下的地图,relative_map主要用于planning模块的参考线生成

### HDmap
HDmap中提供了获取各种高精地图元素的接口，具体的实现在hdmap_impl中.
<br> 
hdmap_impl中将地图元素保存在map中，同时实现了平行于坐标轴的KD-Tree搜索。  
hdmap_impl具体成员如下:
```cpp
  Map map_;
  LaneTable lane_table_;
  JunctionTable junction_table_;
  CrosswalkTable crosswalk_table_;
  SignalTable signal_table_;
  StopSignTable stop_sign_table_;
  YieldSignTable yield_sign_table_;
  ClearAreaTable clear_area_table_;
  SpeedBumpTable speed_bump_table_;
  OverlapTable overlap_table_;
  RoadTable road_table_;
  ParkingSpaceTable parking_space_table_;
  PNCJunctionTable pnc_junction_table_;
  RSUTable rsu_table_;

  std::vector<LaneSegmentBox> lane_segment_boxes_;
  std::unique_ptr<LaneSegmentKDTree> lane_segment_kdtree_;

  std::vector<JunctionPolygonBox> junction_polygon_boxes_;
  std::unique_ptr<JunctionPolygonKDTree> junction_polygon_kdtree_;

  std::vector<CrosswalkPolygonBox> crosswalk_polygon_boxes_;
  std::unique_ptr<CrosswalkPolygonKDTree> crosswalk_polygon_kdtree_;

  std::vector<SignalSegmentBox> signal_segment_boxes_;
  std::unique_ptr<SignalSegmentKDTree> signal_segment_kdtree_;

  std::vector<StopSignSegmentBox> stop_sign_segment_boxes_;
  std::unique_ptr<StopSignSegmentKDTree> stop_sign_segment_kdtree_;

  std::vector<YieldSignSegmentBox> yield_sign_segment_boxes_;
  std::unique_ptr<YieldSignSegmentKDTree> yield_sign_segment_kdtree_;

  std::vector<ClearAreaPolygonBox> clear_area_polygon_boxes_;
  std::unique_ptr<ClearAreaPolygonKDTree> clear_area_polygon_kdtree_;

  std::vector<SpeedBumpSegmentBox> speed_bump_segment_boxes_;
  std::unique_ptr<SpeedBumpSegmentKDTree> speed_bump_segment_kdtree_;

  std::vector<ParkingSpacePolygonBox> parking_space_polygon_boxes_;
  std::unique_ptr<ParkingSpacePolygonKDTree> parking_space_polygon_kdtree_;

  std::vector<PNCJunctionPolygonBox> pnc_junction_polygon_boxes_;
  std::unique_ptr<PNCJunctionPolygonKDTree> pnc_junction_polygon_kdtree_;
```

### pnc_map 
pnc_map 中 具体有cuda_util、path、pnc_map、route_segment<br>
cuda_util中提供了GPU版本的点到最近的线段的实现

#### path 
先看下path中定义的几个数据结构
1. LaneWayPoint包含LaneInfoConstPtr、s、l
2. LaneSegment 包含LaneInfoConstPtr、start_s、end_s ,提供相同ID的LaneSegment的合并。  
3. PathOverlap 表示一条path上被障碍物占据的S区间，包含障碍物的ID、path被占据的起始s和终止s。 
4. MapPathPoint 继承自Vec2d,外加一个朝向以及一组LaneWayPoint(带车道信息，s、l坐标)

pnc map提供的主要的功能有：
1. 更新Routing信息，接收routing的结果，并将结果存储在地图类中。  
2. 短期路径查询，根据routing的结果以及车辆位置，计算当前的可行驶车道区域。
3. 最终路径段生成，根据2的结果生成一条path，供后续参考线生成使用

这里首先回顾下routing的结构。
routing包含一系列的RoadSegments，每个RoadSegment里又是一组Passage，每个Passage中包含一系列的LaneSegments。 

```cpp  
message LaneWaypoint {
  optional string id = 1;
  optional double s = 2;
  optional apollo.common.PointENU pose = 3;
}

message LaneSegment {
  optional string id = 1;
  optional double start_s = 2;
  optional double end_s = 3;
}

message ParkingInfo {
  optional string parking_space_id = 1;
  optional apollo.common.PointENU parking_point = 2;
}

message RoutingRequest {
  optional apollo.common.Header header = 1;
  // at least two points. The first is start point, the end is final point.
  // The routing must go through each point in waypoint.
  repeated LaneWaypoint waypoint = 2;
  repeated LaneSegment blacklisted_lane = 3;
  repeated string blacklisted_road = 4;
  optional bool broadcast = 5 [default = true];
  optional apollo.hdmap.ParkingSpace parking_space = 6 [deprecated = true];
  optional ParkingInfo parking_info = 7;
}

message Measurement {
  optional double distance = 1;
}

enum ChangeLaneType {
  FORWARD = 0;
  LEFT = 1;
  RIGHT = 2;
};

message Passage {
  repeated LaneSegment segment = 1;
  optional bool can_exit = 2;
  optional ChangeLaneType change_lane_type = 3 [default = FORWARD];
}

message RoadSegment {
  optional string id = 1;
  repeated Passage passage = 2;
}

message RoutingResponse {
  optional apollo.common.Header header = 1;
  repeated RoadSegment road = 2;
  optional Measurement measurement = 3;
  optional RoutingRequest routing_request = 4;

  // the map version which is used to build road graph
  optional bytes map_version = 5;
  optional apollo.common.StatusPb status = 6;
}
```
UpdateRoutingResponse函数负责更新routing信息,具体实现如下：
```cpp
    /// file in apollo/modules/map/pnc_map/pnc_map.cc
bool PncMap::UpdateRoutingResponse(const routing::RoutingResponse &routing) {
  range_lane_ids_.clear();
  route_indices_.clear();
  all_lane_ids_.clear();
  // Step 1
  for (int road_index = 0; road_index < routing.road_size(); ++road_index) {
    const auto &road_segment = routing.road(road_index);
    for (int passage_index = 0; passage_index < road_segment.passage_size(); ++passage_index) {
      const auto &passage = road_segment.passage(passage_index);
      for (int lane_index = 0; lane_index < passage.segment_size(); ++lane_index) {
        all_lane_ids_.insert(passage.segment(lane_index).id());
        route_indices_.emplace_back();
        route_indices_.back().segment = ToLaneSegment(passage.segment(lane_index));
        if (route_indices_.back().segment.lane == nullptr) {
          AERROR << "Fail to get lane segment from passage.";
          return false;
        }
        route_indices_.back().index = {road_index, passage_index, lane_index};
      }
    }
  }
```
可以看到以上代码按照RoadSegment、Passage、LaneSegment的三层结构来遍历。结果保存在all_lane_ids_,
route_indices_中。 all_lane_ids_中保存所有车道的ID,route_indices_中的结构为：
```cpp
  struct RouteIndex {
    LaneSegment segment;  //lane id,start_s,end_s 
    std::array<int, 3> index; //{road_index, passage_index, lane_index}
  };
```
参考https://blog.csdn.net/lzw0107/article/details/107814610






