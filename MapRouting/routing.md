## RoutingConfig  
RoutingConfig定义了搜索图中节点和边的cost值
```cpp
message RoutingConfig {
  optional double base_speed = 1;  // base speed for node creator [m/s]
  optional double left_turn_penalty =
      2;  // left turn penalty for node creator [m]
  optional double right_turn_penalty =
      3;                              // right turn penalty for node creator [m]
  optional double uturn_penalty = 4;  // left turn penalty for node creator [m]
  optional double change_penalty = 5;  // change penalty for edge creator [m]
  optional double base_changing_length =
      6;  // base change length penalty for edge creator [m]
  optional TopicConfig topic_config = 7;
}

```
RoutingConfig默认加载的值如下:
```cpp  
base_speed: 4.167 
left_turn_penalty: 50.0
right_turn_penalty: 20.0
uturn_penalty: 100.0
change_penalty: 500.0
base_changing_length: 50.0
topic_config {
  routing_response_topic: "/apollo/routing_response"
  routing_response_history_topic: "/apollo/routing_response_history"
}
```
搜索图节点cost计算
```cpp
  double lane_length = GetLaneLength(lane);
  double speed_limit =
      lane.has_speed_limit() ? lane.speed_limit() : routing_config.base_speed();
  double ratio = speed_limit >= routing_config.base_speed()
                     ? std::sqrt(routing_config.base_speed() / speed_limit)
                     : 1.0;
  double cost = lane_length * ratio;  // 道路长度                            
  if (lane.has_turn()) {
    if (lane.turn() == Lane::LEFT_TURN) { // 车道类型 
      cost += routing_config.left_turn_penalty();
    } else if (lane.turn() == Lane::RIGHT_TURN) {
      cost += routing_config.right_turn_penalty();
    } else if (lane.turn() == Lane::U_TURN) {
      cost += routing_config.uturn_penalty();
    }
  }
```
