## PieceWiseJerk优化
中文名字叫分段加加速度优化，主要作用是在SL坐标系下，给定初始状态(x,x',x''),(根据问题不同末状态可以指定，可以不指定),以及一系列的边界约束,优化出一条平滑曲线。
在SL中可以用于路径的平滑优化，在ST中可以用于速度的平滑优化。
### 算法的输入输出
输入：
1. 光滑的参考线
2. 当前车辆的运动状态($x,y,\theta,k,v,a$)
3. 静态障碍物信息，包括位置、朝向、尺寸
4. 车辆配置信息，如尺寸、轴距、最大转向角及转向速率等。
5. 连续曲率的平滑行驶路径

输出：
1. 曲率连续的平滑路径

### 算法流程
1. 将环境的静态障碍物映射到frenet坐标系下

![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/PieceWiseJerk1.PNG)
本质上，路径规划问题实际上是求解l关于s的函数。

frenet 参考文献：
1. Werling M , Ziegler J , Kammel S , et al. Optimal Trajectory Generation for Dynamic Street Scenarios in a Frenet Frame[C]// Robotics and Automation (ICRA), 2010 IEEE International Conference on. IEEE, 2010.
2. Julian Schlechtriemen, Kim Peter Wabersich, Klaus-Dieter Kuhnert. Wiggling through complex traffic: Planning trajectories constrained by predictions[C]// Intelligent Vehicles Symposium. IEEE, 2016.


2. frenet坐标系离散化
将要优化的曲线等距离划分成多段(SL中根据S划分，ST中根据T划分)，计算每个离散点s处的边界($l_{min}^{i}$,$l_{max}^{i}$)
这里简化问题把车辆看做一个点，为了避免碰撞，在计算可行域的边界时候可以加一定的缓冲余量。

![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/PieceWiseJerk2.PNG)

3. 选择优化变量
在路径优化中，我们选择横向偏移以及它们的一阶二阶导数$l_i$,${l_i}^{'}$，${l_i}^{''}$作为优化变量,物理意义是横向偏移小,以及横向偏移的趋势小(类似于速度加速度)
该算法中有一个假设：每一段中jerk不变，即相邻两点之间${l_{i \rightarrow i+1}}^{'''}$为定值，该值可以由相邻两点的二阶导数差分算得，在优化过程中${l_{i \rightarrow i+1}}^{'''}$会随着相邻点的${l_i}^{''}$ -> $l_{i+1}^{''}$变化而变化，但是在两个点之间的这一段为固定不变。 
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/PieceWiseJerk3.PNG)

4. 设置优化目标
为了保证路径的舒适性，希望路径的$l_i$,${l_i}^{'}$，${l_i}^{''}$尽可能小。
这里我们要优化的点为n个，每个点有三个变量要优化，所以要优化的变量个数为3n,对应于QP中的核矩阵P的维度为3n*3n。
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/PieceWiseJerk5.PNG)

优化的目标函数为：
平滑项
$$
J_{smooth} = w_{l} \sum_{i=0}^{n-1} {l_{i}}^2  +   w_{l^{'}} \sum_{i=0}^{n-1}  {l_i^{'}}^2  +  w_{l^{''}} \sum_{i=0}^{n-1}  {l_i^{''}}^2 +   w_{l^{'''}} \sum_{i=0}^{n-2}  {l_{i \rightarrow i+1}^{'''}}^2
$$
中心线(该项为Apollo新加项，由于QP在满足边界约束时候就有解,有时候会离障碍物很近)
$$
J_{obs} = w_{obs} \sum_{i=0}^{n-1} {(l_{i}-0.5(lb+rb))}^2 
$$
终点状态误差项
$$
J_{end} = w_{end1}(l_{n-1}- l_{endref})^2 + w_{end2}(l_{n-1}^{'}- l_{endref}^{'})^2 +  w_{end3}(l_{n-1}^{''}- l_{endref}^{''})^2 
$$
https://www.cnblogs.com/icathianrain/p/14407626.html
等式约束推导：　
每一段看做三次多项式，每一段的３阶导数为常量. 





这里以三个点为例，将优化函数表达成矩阵形式。
先计算前三项得到：
$$
\begin{bmatrix} 
 x_0 & x_1 & x_2 & x_0^{'} & x_1^{'} & x_2^{'} &  x_0^{''}  & x_1^{''} & x_2^{''} 
 \end{bmatrix}
 \begin{bmatrix}
 w_l & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
 0 & w_l & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
 0 & 0 & w_l & 0 & 0 & 0 & 0 & 0 & 0 \\
 0 & 0 & 0 & w_{l^{'}} & 0 & 0 & 0 & 0 & 0 \\
 0 & 0 & 0 & 0 &  w_{l^{'}} & 0 & 0 & 0 & 0 \\
 0 & 0 & 0 & 0 & 0 &  w_{l^{'}} & 0 & 0 & 0 \\
 0 & 0 & 0 & 0 & 0 & 0 & w_{l^{''}} & 0 & 0 \\
 0 & 0 & 0 & 0 & 0 & 0 & 0 & w_{l^{''}} & 0 \\
 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & w_{l^{''}} \\
 \end{bmatrix}
 \begin{bmatrix}
 x_0 \\ x_1 \\ x_2 \\ x_0^{'} \\ x_1^{'} \\ x_2^{'} \\  x_0^{''}  \\ x_1^{''} \\ x_2^{''} 
 \end{bmatrix}
$$
对于第4项三阶导的项：
$$ 
 w_{l^{'''}} * {{l_{i \rightarrow i+1}}^{'''}}^2 =  \frac{ w_{l^{'''}} *(l_{i}^{''})^2+ w_{l^{'''}} *(l_{i+1}^{''})^2   - 2 w_{l^{'''}} * (l_i^{''}*l_{i+1}^{''})}{(\triangle s)^2}
$$
注意这里第一个数据点和最后一个点是加一次，中间的各个数据点是加2次。
所以从$x_0$ 和 $x_{n-1}$对应的二阶导 系数是：
$$
w_l^{''} + \frac{w_l^{'''}}{{\triangle s}^2} 
$$  
从$x_1$到$x_{n-1}$的各项的二阶导系数为：
$$
w_l^{''} + \frac{2w_l^{'''}}{{\triangle s}^2} 
$$ 
另外，上述公式中的二阶导，除了平方项外还有$l_i^{''}*l_{i+1}^{''}$项，其系数为：
 $$ \frac{ -2w_l^{'''} }{{\triangle s}^2}$$

所以上述矩阵公式更新为：
$$
\begin{bmatrix} 
 x_0 & x_1 & x_2 & x_0^{'} & x_1^{'} & x_2^{'} &  x_0^{''}  & x_1^{''} & x_2^{''} 
 \end{bmatrix}
 \begin{bmatrix}
 w_l & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
 0 & w_l & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
 0 & 0 & w_l & 0 & 0 & 0 & 0 & 0 & 0 \\
 0 & 0 & 0 & w_{l^{'}} & 0 & 0 & 0 & 0 & 0 \\
 0 & 0 & 0 & 0 &  w_{l^{'}} & 0 & 0 & 0 & 0 \\
 0 & 0 & 0 & 0 & 0 &  w_{l^{'}} & 0 & 0 & 0 \\
 0 & 0 & 0 & 0 & 0 & 0 & w_l^{''} + \frac{w_l^{'''}}{{\triangle s}^2}  & \frac{ -2w_l^{'''} }{{\triangle s}^2} & 0 \\
 0 & 0 & 0 & 0 & 0 & 0 & 0 & w_l^{''} + \frac{2w_l^{'''}}{{\triangle s}^2}& \frac{ -2w_l^{'''} }{{\triangle s}^2} \\
 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 &  w_l^{''} + \frac{w_l^{'''}}{{\triangle s}^2} \\
 \end{bmatrix}
 \begin{bmatrix}
 x_0 \\ x_1 \\ x_2 \\ x_0^{'} \\ x_1^{'} \\ x_2^{'} \\  x_0^{''}  \\ x_1^{''} \\ x_2^{''} 
 \end{bmatrix}
$$

该矩阵的非零元素个数为: $3n+(n-1) = 4n-1$
注意此处OSQP要求矩阵P是上三角矩阵，注意二阶交叉项的下标位置。

5. offset矩阵的确定
offset矩阵表示设置的参考值的系数。
$$
q = \begin{bmatrix} 
 -2 w_{x_ref} *xref_0 \\ -2 -2 w_{x_ref} *xref_1 \\ -2 -2 w_{x_ref} *xref_2 \\ -2 w_{dx_ref} *dxref \\ -2 w_{dx_ref} *dxref \\ -2 w_{dx_ref} *dxref  \\  -2 w_l *xref_0 \\ -2 w_l *xref_1 \\ -2 w_l *xref_2 
 \end{bmatrix}
$$

6. 设置约束条件
   1. 三阶导(二阶导差分)约束:
   $$
   -0.5 \leq {x_{i+1}}^{''} - {x_{i}}^{''} \leq 0.5
   $$
   对应到约束矩阵就是第i行的第2*n+i列为-1，2*n+i+1列为1；
   2. 一阶导连续 
    $$
    {x_{i+1}}^{'} - {x_{i}}^{'} - 0.5 * \triangle{s} * {x_i}^{''} - 0.5 * \triangle{s} *{x_{i+1}}^{''} = 0  
    $$
   3. 0阶导连续

    $$
       x_{i+1} - x_{i} - \triangle{s} * {x_{i}}^{'} - \frac{1}{3} * {\triangle{s}}^2 *{x_i}^{''}  - \frac{1}{6}  {\triangle{s}}^2  {x_{i+1}}^{''}
    $$

    4. 初始状态、末状态约束 
    $$
        x_{init} 的0，1，2阶导数
        x_{end}  的0,1,2阶导数
    $$
上述优化目标和约束最终转换为标准的QP问题形式：
 $$
    \min \frac{1}{2} X^{T}HX+X^{T}g  \\
    \\
    lbA \leq{AX}\leq{ubA}  \\
    lb \leq{X}\leq{ub}
$$
Apollo中使用OSQP求解的代码有点问题，这里我们是用qpOASES求解。

最终的优化曲线如下：
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/PieceWiseJerk4.PNG)



