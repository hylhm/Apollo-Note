## 梯度下降轨迹平滑
https://zhuanlan.zhihu.com/p/135441089


![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/GD_trajectory.PNG)

梯度优化的结果:
weight_ref = 0.5,weight_smooth=0.7时优化结果
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/traj_smooth_1.PNG)
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/traj_smooth_2.PNG)

weight_ref = 0.4,weight_smooth=0.7时优化结果
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/traj_smooth_3.PNG)
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/traj_smooth_4.PNG)

Apollo离散轨迹点平滑有两种方法：
1. fem_pos_deviation 
2. cos_theta 


## FemPosDeviationOsqp
变量个数:  num_points*2 (x和y两维)
约束个数： num_points*2

主要步骤：
1. 计算核矩阵
   1. 有限元估计：点到中点的距离 
       $$
       cost_{fem} = w_{ref_deviation}{(x_0+x_2-2x_1)^2 + (y_0+y_2-2y_1)^2}
       $$
       这个惩罚项的物理意义：从中间那个点到第一个点的向量 和 从中间那个点到最后一个点的向量 的矢量和的模的平方。显然，如果这三个点在一条直线上，那么这个值最小；三个点组成的两个线段的夹角越小，即曲线越“弯”，这个值就越大。所以这一项是跟参考线的平滑程度最相关的。
   2. path长度惩罚项
      $$
       cost_{path_length} = w_{path_length}{(x_1-x_0)^2 + (y_1+y_0)^2} 
      $$
   3. 和参考点的距离惩罚
      $$
      cost_{ref} = w_{ref}{(x_i-x_{ref})^2 + (y_i+y_{ref})^2}
      $$
   4. 松弛变量  这个还没搞懂。
   // TODO: 使用IPOPT求解NLP问题。 
最终的核矩阵为：
```cpp
  // |X+Y+Z, -2X-Y,   X,       0,       0,       0    |
  // |0,     5X+2Y+Z, -4X-Y,   X,       0,       0    |
  // |0,     0,       6X+2Y+Z, -4X-Y,   X,       0    |
  // |0,     0,       0,       6X+2Y+Z, -4X-Y,   X    |
  // |0,     0,       0,       0,       5X+2Y+Z, -2X-Y|
  // |0,     0,       0,       0,       0,       X+Y+Z|
```
这里每个元素都是一个2*2的分块矩阵。
2. 计算约束矩阵
   这个约束就是每一项的边界约束，在参考点上给一个上下的偏差。
3. 计算offset
   每一项为-2 *w_ref* ref
4. 指定warm start
   1. 就是初始的x,y坐标
![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/traj_smooth_5.PNG)

### CosTheta 
   目标函数
   P0(x0,y0),P1(x1,y1),P2(x2,y2)为连续的三个点。
   cost的主要是向量p1p0和p1p2的余弦值。
   $$
   cost = \frac{(x_1-x_0)(x_2-x_1) +(y1-y0)(y_2-y_1)}{\sqrt{(x_1-x_0)^2 +(y_1-y_0)^2} *\sqrt{(x_2-x_1)^2+(y_2-y_1)^2}}
   $$
Apollo中使用了非线性优化来求解该优化问题。
//TODO: IPOPT非线性优化库。  
### IterativeAnchoringSmoother
迭代Anchor优化：
1. QP对Path进行Smooth 
2. 每次Smooth后检查碰撞，将发生碰撞的点标记为Anchor Point，优化的时候这些点不动。


Smooth的主要流程：
```cpp
bool IterativeAnchoringSmoother::Smooth(
    const Eigen::MatrixXd& xWS, const double init_a, const double init_v,
    const std::vector<std::vector<Vec2d>>& obstacles_vertices_vec,
    DiscretizedTrajectory* discretized_trajectory) 
```
xWS为输入的状态，每一行为状态的一维，每一列为一组数据。
discretized_trajectory为保存输出轨迹的指针。
1. 设置档位
2. 设置障碍物信息
3. 初始轨迹赋值x，y,theta,s
4. 依据设置的delta_s对轨迹进行重新插值，使得间距均匀。
5. 调整初始和末位置朝向，使得heading连续，依据第一个点和最后一个点的朝向求出第二和倒数第二个点。




档位判断：依据初始朝向和前两个点位移方向。
```cpp
bool IterativeAnchoringSmoother::CheckGear(const Eigen::MatrixXd& xWS) {
  CHECK_GT(xWS.size(), 1);
  double init_heading_angle = xWS(2, 0);
  const Vec2d init_tracking_vector(xWS(0, 1) - xWS(0, 0),
                                   xWS(1, 1) - xWS(1, 0));
  double init_tracking_angle = init_tracking_vector.Angle();
  return std::abs(NormalizeAngle(init_tracking_angle - init_heading_angle)) <
         M_PI_2;
}

```




