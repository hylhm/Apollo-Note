# Apollo Planning 模块概述
目录结构:
```
├── common  
├── conf
├── constraint_checker
├── dag
├── data
├── images
├── integration_tests
├── lattice
├── launch
├── math
├── navi
├── open_space
├── planner
├── proto
├── reference_line
├── scenarios
├── tasks
├── testdata
├── traffic_rules
└── tuning

```
## common 
```cpp
├── path
├── speed
├── trajectory
├── trajectory1d
└── util
```

### util 
```cpp
├── BUILD
├── common.cc
├── common.h
├── util.cc
└── util.h
```
planning::common里只提供了一个添加虚拟障碍物的功能：

```cpp
//添加虚拟障碍物以及stop的决策
BuildStopDecision(const std::string& stop_wall_id, const double stop_line_s,
                      const double stop_distance,
                      const StopReasonCode& stop_reason_code,
                      const std::vector<std::string>& wait_for_obstacles,
                      const std::string& decision_tag, Frame* const frame,
                      ReferenceLineInfo* const reference_line_info);
```
planning::util 
```cpp
// 对车辆的x,y,z,heading,kappa,liner_velocity,linear_acceleration做检查,检查是不是nan
bool IsVehicleStateValid(const apollo::common::VehicleState& vehicle_state);
// 通过Route的header判断是不是不同route
bool IsDifferentRouting(const apollo::routing::RoutingResponse& first,
                        const apollo::routing::RoutingResponse& second);
//根据停车距离计算停车时候的Deceleration。v0*v0/2s  
double GetADCStopDeceleration(const double adc_front_edge_s,
                              const double stop_line_s);
//检查stop sign是不是还在当前参考线上,根据stop_sign_overlaps进行线性查找。
bool CheckStopSignOnReferenceLine(const ReferenceLineInfo& reference_line_info,
                                  const std::string& stop_sign_overlap_id);
//检查交通灯，方法同上。
bool CheckTrafficLightOnReferenceLine(
    const ReferenceLineInfo& reference_line_info,
    const std::string& traffic_light_overlap_id);
//判断当前车辆是不是在Junction里面，根据参考线和自车的s查找，距离阈值超过2米则认为离开junction
bool CheckInsidePnCJunction(const ReferenceLineInfo& reference_line_info);
```

### path 
```cpp
├── BUILD
├── discretized_path.cc
├── discretized_path.h
├── discretized_path_test.cc
├── frenet_frame_path.cc
├── frenet_frame_path.h
├── frenet_frame_path_test.cc
├── path_data.cc
└── path_data.h
```
path里包含了discretized_path和frenet_frame_path
```cpp
FrenetFramePath : public std::vector<common::FrenetFramePoint>;
DiscretizedPath : public std::vector<common::PathPoint>;
```
PathData包含了XY坐标系和SL坐标系转换,参考线信息。
PathPoint的定义如下:
```cpp
message PathPoint {
  // coordinates
  optional double x = 1;
  optional double y = 2;
  optional double z = 3;

  // direction on the x-y plane
  optional double theta = 4;
  // curvature on the x-y planning
  optional double kappa = 5;
  // accumulated distance from beginning of the path
  optional double s = 6;

  // derivative of kappa w.r.t s.
  optional double dkappa = 7;
  // derivative of derivative of kappa w.r.t s.
  optional double ddkappa = 8;
  // The lane ID where the path point is on
  optional string lane_id = 9;

  // derivative of x and y w.r.t parametric parameter t in CosThetareferenceline
  optional double x_derivative = 10;
  optional double y_derivative = 11;
}
```
### speed 
```cpp
├── speed_data.cc
├── speed_data.h
├── st_boundary.cc
├── st_boundary.h
├── st_boundary_test.cc
├── st_point.cc
└── st_point.h
```
st_point比较简单，st_boundary为一组st_point组成的边界。
主要关注SpeedData 
```cpp
SpeedData : public std::vector<common::SpeedPoint>;
//构造函数
explicit SpeedData(std::vector<common::SpeedPoint> speed_points);
//添加速度点
void AppendSpeedPoint(const double s, const double time, const double v,
                        const double a, const double da);
//根据时间匹配速度点，二分查找时间最近的点，之后再进行线性插值。                        
bool EvaluateByTime(const double time,
                      common::SpeedPoint* const speed_point) const;

  // Assuming spatial traversed distance is monotonous, which is the case for
  // current usage on city driving scenario
// 根据S查找速度点，方法同上。
bool EvaluateByS(const double s, common::SpeedPoint* const speed_point) const;
// back().t() - front().t()
double TotalTime() const;

// Assuming spatial traversed distance is monotonous
//// back().s() - front().s()
double TotalLength() const;

// SpeedPoint 具体内容如下:
message SpeedPoint {
  optional double s = 1;
  optional double t = 2;
  // speed (m/s)
  optional double v = 3;
  // acceleration (m/s^2)
  optional double a = 4;
  // jerk (m/s^3)
  optional double da = 5;
}
```
### trajectory
```cpp
├── BUILD
├── discretized_trajectory.cc
├── discretized_trajectory.h
├── discretized_trajectory_test.cc
├── publishable_trajectory.cc
├── publishable_trajectory.h
└── publishable_trajectory_test.cc
```
继承关系:
```cpp
DiscretizedTrajectory : public std::vector<common::TrajectoryPoint>;
PublishableTrajectory : public DiscretizedTrajectory;
```
PublishableTrajectory是planning中定义的发布出去的轨迹。
轨迹点的定义如下:
```cpp
message TrajectoryPoint {
  // path point
  optional PathPoint path_point = 1; // x,y,heading,
  // linear velocity
  optional double v = 2;  // in [m/s]
  // linear acceleration
  optional double a = 3;
  // relative time from beginning of the trajectory
  optional double relative_time = 4;
  // longitudinal jerk
  optional double da = 5;
  // The angle between vehicle front wheel and vehicle longitudinal axis
  optional double steer = 6;
}
```

### trajectory1d
轨迹都继承自Curve1D，

目前Apollo的轨迹有：
1. 
