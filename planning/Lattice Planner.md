## Apollo Lattice  
lattice的文件结构 
```cpp
├── behavior
│   ├── BUILD
│   ├── feasible_region.cc
│   ├── feasible_region.h
│   ├── path_time_graph.cc
│   ├── path_time_graph.h
│   ├── prediction_querier.cc
│   └── prediction_querier.h
└── trajectory_generation
    ├── backup_trajectory_generator.cc
    ├── backup_trajectory_generator.h
    ├── BUILD
    ├── end_condition_sampler.cc
    ├── end_condition_sampler.h
    ├── lateral_osqp_optimizer.cc
    ├── lateral_osqp_optimizer.h
    ├── lateral_qp_optimizer.cc
    ├── lateral_qp_optimizer.h
    ├── lattice_trajectory1d.cc
    ├── lattice_trajectory1d.h
    ├── piecewise_braking_trajectory_generator.cc
    ├── piecewise_braking_trajectory_generator.h
    ├── trajectory1d_generator.cc
    ├── trajectory1d_generator.h
    ├── trajectory_combiner.cc
    ├── trajectory_combiner.h
    ├── trajectory_evaluator.cc
    └── trajectory_evaluator.h
```
### FeasibleRegion 
给定初始s,v，a根据纵向加速度的上界、下界计算S、V、T的上下界。(使用恒减速度模型计算)。
```cpp 
class FeasibleRegion {
 public:
  explicit FeasibleRegion(const std::array<double, 3>& init_s);

  double SUpper(const double t) const;

  double SLower(const double t) const;

  double VUpper(const double t) const;

  double VLower(const double t) const;

  double TLower(const double s) const;

 private:
  std::array<double, 3> init_s_;

  double t_at_zero_speed_;

  double s_at_zero_speed_;
};
```

### PathTimeGraph
ST 图和SL图的实现。
私有成员有：
```cpp
 private:
  std::pair<double, double> time_range_; // T的范围 
  std::pair<double, double> path_range_; // S的范围 
  const ReferenceLineInfo* ptr_reference_line_info_;
  std::array<double, 3> init_d_;
  std::unordered_map<std::string, STBoundary> path_time_obstacle_map_;  //障碍物的SL范围
  std::vector<STBoundary> path_time_obstacles_;
  std::vector<SLBoundary> static_obs_sl_boundaries_;
```
复习下之前的参考线，参考线包括：
1. 一组限速 
2. 一组参考点(x,y,theta,kappa,dkappa) std::vector<ReferencePoint> reference_points_;
3. hdmap::Path (包含WayPoint和LaneSegment信息) hdmap::Path map_path_;
4. 优先级(在PublicRoad中没有用到) 
reference_points_是根据map_path计算得到的，所以2和3作用相同。 
ReferenceLineInfo 包含一条参考线的所有数据，包括障碍物数据。

PathTimeGraph构造函数
```cpp
PathTimeGraph::PathTimeGraph(
    const std::vector<const Obstacle*>& obstacles,
    const std::vector<PathPoint>& discretized_ref_points,
    const ReferenceLineInfo* ptr_reference_line_info, const double s_start,
    const double s_end, const double t_start, const double t_end,
    const std::array<double, 3>& init_d) {
  CHECK_LT(s_start, s_end);
  CHECK_LT(t_start, t_end);
  path_range_.first = s_start;
  path_range_.second = s_end;
  time_range_.first = t_start;
  time_range_.second = t_end;
  ptr_reference_line_info_ = ptr_reference_line_info;
  init_d_ = init_d;

  SetupObstacles(obstacles, discretized_ref_points);
}
```
构造函数首先进行变量赋值，然后SetupObstacles。
SetupObstacles计算动静态障碍物的SL边界，并对静态障碍物进行S排序。动态障碍物计算ST。
ComputeObstacleBoundary把障碍物的所有多边形点投影到参考线上计算SL坐标。

### PredictionQuerier
主要提供了查询障碍物沿参考线的速度。具体如下：
double PredictionQuerier::ProjectVelocityAlongReferenceLine(
    const std::string& obstacle_id, const double s, const double t) const {
  CHECK(id_obstacle_map_.find(obstacle_id) != id_obstacle_map_.end());
  //障碍物轨迹过短或者查询的时间不在障碍物轨迹区间返回0
  const auto& trajectory = id_obstacle_map_.at(obstacle_id)->Trajectory();
  int num_traj_point = trajectory.trajectory_point_size();
  if (num_traj_point < 2) {
    return 0.0;
  }

  if (t < trajectory.trajectory_point(0).relative_time() ||
      t > trajectory.trajectory_point(num_traj_point - 1).relative_time()) {
    return 0.0;
  }
//找到时间t对应的轨迹点
  auto matched_it =
      std::lower_bound(trajectory.trajectory_point().begin(),
                       trajectory.trajectory_point().end(), t,
                       [](const common::TrajectoryPoint& p, const double t) {
                         return p.relative_time() < t;
                       });
  //将速度分解到ENU坐标系的vx和vy
  double v = matched_it->v();
  double theta = matched_it->path_point().theta();
  double v_x = v * std::cos(theta);
  double v_y = v * std::sin(theta);
  // 计算对应的参考线投影点
  common::PathPoint obstacle_point_on_ref_line =
      common::math::PathMatcher::MatchToPath(*ptr_reference_line_, s);
  auto ref_theta = obstacle_point_on_ref_line.theta();
  //合成参考线方向的速度
  return std::cos(ref_theta) * v_x + std::sin(ref_theta) * v_y;
}

## lattice 
lattice主要有以下几个部分：
1. 末状态采样
2. 轨迹生成
3. 轨迹评估
4. 轨迹优化
5. 横纵向轨迹合并

### EndConditionSampler
```cpp
std::array<double, 3> init_s_;   //初始S状态
std::array<double, 3> init_d_;   //初始D状态
FeasibleRegion feasible_region_;  //计算s、v、t的上下界。
std::shared_ptr<PathTimeGraph> ptr_path_time_graph_; //障碍物SL信息。
std::shared_ptr<PredictionQuerier> ptr_prediction_querier_;  //障碍物投影速度查询
```
**构造函数** 
```cpp
EndConditionSampler::EndConditionSampler(
    const State& init_s, const State& init_d,
    std::shared_ptr<PathTimeGraph> ptr_path_time_graph,
    std::shared_ptr<PredictionQuerier> ptr_prediction_querier)
    : init_s_(init_s),
      init_d_(init_d),
      feasible_region_(init_s),
      ptr_path_time_graph_(std::move(ptr_path_time_graph)),
      ptr_prediction_querier_(std::move(ptr_prediction_querier)) {}
```
这里需要注意的两个数据类型：
using State = std::array<double, 3>; // 纵向可以表示s,v,a;横向可以表示l,dl,ddl
using Condition = std::pair<State, double>; //纵向可以表示一个t对应的纵向状态;横向可以表示1个s对应的l,dl,ddl

**横向轨迹采样**
采样一组s对应的横向State,S采样值为{10,20,40,80},l采样为{0,-0.5,0.5}。
```cpp
std::vector<Condition> EndConditionSampler::SampleLatEndConditions() const {
  std::vector<Condition> end_d_conditions;
  std::array<double, 3> end_d_candidates = {0.0, -0.5, 0.5};
  std::array<double, 4> end_s_candidates = {10.0, 20.0, 40.0, 80.0};

  for (const auto& s : end_s_candidates) {
    for (const auto& d : end_d_candidates) {
      State end_d_state = {d, 0.0, 0.0};
      end_d_conditions.emplace_back(end_d_state, s);
    }
  }
  return end_d_conditions;
}
```
**纵向轨迹采样** 
curise采样:

```cpp  
std::vector<Condition> EndConditionSampler::SampleLonEndConditionsForCruising(
    const double ref_cruise_speed) const {
  CHECK_GT(FLAGS_num_velocity_sample, 1); // 默认值6，速度采样数量 

  // time interval is one second plus the last one 0.01
  constexpr size_t num_of_time_samples = 9;
  std::array<double, num_of_time_samples> time_samples;
  for (size_t i = 1; i < num_of_time_samples; ++i) {
    auto ratio =
        static_cast<double>(i) / static_cast<double>(num_of_time_samples - 1);
    time_samples[i] = FLAGS_trajectory_time_length * ratio;  //轨迹时间长度,默认8秒
  }
  time_samples[0] = FLAGS_polynomial_minimal_param;   //0.01,不从0开始，可以有效地避免负的t出现

  std::vector<Condition> end_s_conditions;
  for (const auto& time : time_samples) {   //feasible_region_ 给定初始State,计算时间t的速度上下界
    double v_upper = std::min(feasible_region_.VUpper(time), ref_cruise_speed);
    double v_lower = feasible_region_.VLower(time);

    State lower_end_s = {0.0, v_lower, 0.0};
    end_s_conditions.emplace_back(lower_end_s, time);

    State upper_end_s = {0.0, v_upper, 0.0};
    end_s_conditions.emplace_back(upper_end_s, time);

    double v_range = v_upper - v_lower;
    // Number of sample velocities
    size_t num_of_mid_points =
        std::min(static_cast<size_t>(FLAGS_num_velocity_sample - 2),
                 static_cast<size_t>(v_range / FLAGS_min_velocity_sample_gap)); //FLAGS_min_velocity_sample_gap 默认1

    if (num_of_mid_points > 0) {
      double velocity_seg =
          v_range / static_cast<double>(num_of_mid_points + 1);
      for (size_t i = 1; i <= num_of_mid_points; ++i) {
        State end_s = {0.0, v_lower + velocity_seg * static_cast<double>(i),
                       0.0};
        end_s_conditions.emplace_back(end_s, time);
      }
    }
  }
  return end_s_conditions;   // end_s_conditions中只采样了v，s和a为0；
}
```
stop采样:
stop 采样中的每个t对应的State只包含S，v和a为0 
```cpp
std::vector<Condition> EndConditionSampler::SampleLonEndConditionsForStopping(
    const double ref_stop_point) const {
  // time interval is one second plus the last one 0.01
  constexpr size_t num_of_time_samples = 9;
  std::array<double, num_of_time_samples> time_samples;
  for (size_t i = 1; i < num_of_time_samples; ++i) {
    auto ratio =
        static_cast<double>(i) / static_cast<double>(num_of_time_samples - 1);
    time_samples[i] = FLAGS_trajectory_time_length * ratio;
  }
  time_samples[0] = FLAGS_polynomial_minimal_param;

  std::vector<Condition> end_s_conditions;
  for (const auto& time : time_samples) {
    State end_s = {std::max(init_s_[0], ref_stop_point), 0.0, 0.0};
    end_s_conditions.emplace_back(end_s, time);
  }
  return end_s_conditions;
}
```
关于障碍物的采样
对每个障碍物分别生成follow和overtake轨迹，具体的做法是在目标障碍物s的前后分别采样。
```cpp
std::vector<SamplePoint>
EndConditionSampler::QueryPathTimeObstacleSamplePoints() const {
  const auto& vehicle_config =
      common::VehicleConfigHelper::Instance()->GetConfig();
  std::vector<SamplePoint> sample_points;  //SamplePoint包含s参考速度v_ref 
  for (const auto& path_time_obstacle :
       ptr_path_time_graph_->GetPathTimeObstacles()) {
    std::string obstacle_id = path_time_obstacle.id();
    QueryFollowPathTimePoints(vehicle_config, obstacle_id, &sample_points);
    QueryOvertakePathTimePoints(vehicle_config, obstacle_id, &sample_points);
  }
  return sample_points;
}
```
over take和follow的采样在S-T图中体现在障碍物的上方和下方。
```cpp
std::vector<STPoint> PathTimeGraph::GetObstacleSurroundingPoints(
    const std::string& obstacle_id, const double s_dist,
    const double t_min_density) const {
  CHECK(t_min_density > 0.0);
  std::vector<STPoint> pt_pairs;
  if (path_time_obstacle_map_.find(obstacle_id) ==
      path_time_obstacle_map_.end()) {
    return pt_pairs;
  }

  const auto& pt_obstacle = path_time_obstacle_map_.at(obstacle_id);

  double s0 = 0.0;
  double s1 = 0.0;

  double t0 = 0.0;
  double t1 = 0.0;
  if (s_dist > 0.0) {
    s0 = pt_obstacle.upper_left_point().s();
    s1 = pt_obstacle.upper_right_point().s();

    t0 = pt_obstacle.upper_left_point().t();
    t1 = pt_obstacle.upper_right_point().t();
  } else {
    s0 = pt_obstacle.bottom_left_point().s();
    s1 = pt_obstacle.bottom_right_point().s();

    t0 = pt_obstacle.bottom_left_point().t();
    t1 = pt_obstacle.bottom_right_point().t();
  }

  double time_gap = t1 - t0;
  CHECK(time_gap > -FLAGS_numerical_epsilon);
  time_gap = std::fabs(time_gap);

  size_t num_sections = static_cast<size_t>(time_gap / t_min_density + 1);
  double t_interval = time_gap / static_cast<double>(num_sections);

  for (size_t i = 0; i <= num_sections; ++i) {
    double t = t_interval * static_cast<double>(i) + t0;
    double s = lerp(s0, t0, s1, t1, t) + s_dist;

    STPoint ptt;
    ptt.set_t(t);
    ptt.set_s(s);
    pt_pairs.push_back(std::move(ptt));
  }

  return pt_pairs;
}
```

### LatticeTrajectory1d 
继承自Curve1d,私有数据成员:  
```cpp
  std::shared_ptr<Curve1d> ptr_trajectory1d_;
  double target_position_ = 0.0;
  double target_velocity_ = 0.0;
  double target_time_ = 0.0;
  bool has_target_position_ = false;
  bool has_target_velocity_ = false;
  bool has_target_time_ = false;
```

Evaluate函数在param小于ParamLength()时执行ptr_trajectory1d_的Evaluate，当param超出ParamLength()时,使用匀加速模型进行延长。

### Trajectory1dGenerator
私有成员
```cpp
  std::array<double, 3> init_lon_state_;  //纵向初始状态
  std::array<double, 3> init_lat_state_;  //横向初始状态
  EndConditionSampler end_condition_sampler_;  //末状态采样器
  std::shared_ptr<PathTimeGraph> ptr_path_time_graph_;  // 障碍物ST图
```
生成横纵向轨迹
```cpp
void Trajectory1dGenerator::GenerateTrajectoryBundles(
    const PlanningTarget& planning_target,  //proto中定义的，包含停车点的s和
    Trajectory1DBundle* ptr_lon_trajectory_bundle,
    Trajectory1DBundle* ptr_lat_trajectory_bundle) {
  GenerateLongitudinalTrajectoryBundle(planning_target,
                                       ptr_lon_trajectory_bundle);

  GenerateLateralTrajectoryBundle(ptr_lat_trajectory_bundle);
  return;
}
```

## 轨迹生成
### 纵向轨迹生成
分成cruise、follow、overtake、stop












