## PlanningComponent

该类继承自Component,并接收PredictionObstacles、Chassis、LocalizationEstimate消息。
```
class PlanningComponent final
    : public cyber::Component<prediction::PredictionObstacles, canbus::Chassis,
                              localization::LocalizationEstimate>{}
```

CYBER_REGISTER_COMPONENT(PlanningComponent)  //注册该Component


### 主要reader和writer

除以上三个默认的消息，还有其他4个的reader：

```
std::shared_ptr<cyber::Reader<perception::TrafficLightDetection>>
      traffic_light_reader_; //交通灯检测结果
std::shared_ptr<cyber::Reader<routing::RoutingResponse>> routing_reader_; // routing结果
std::shared_ptr<cyber::Reader<planning::PadMessage>> pad_msg_reader_;     // 这个是干嘛的?
std::shared_ptr<cyber::Reader<relative_map::MapMsg>> relative_map_reader_;// 相对地图，用于没有高精地图场景
```
planning模块的输出数据有两个，发起reroute的请求和发给控制的轨迹数据。

```
std::shared_ptr<cyber::Writer<ADCTrajectory>> planning_writer_;
std::shared_ptr<cyber::Writer<routing::RoutingRequest>> rerouting_writer_;

```
### 主要成员

```
private:
  // reader和writer
  std::shared_ptr<cyber::Reader<perception::TrafficLightDetection>>
      traffic_light_reader_;
  std::shared_ptr<cyber::Reader<routing::RoutingResponse>> routing_reader_;
  std::shared_ptr<cyber::Reader<planning::PadMessage>> pad_msg_reader_;
  std::shared_ptr<cyber::Reader<relative_map::MapMsg>> relative_map_reader_;

  std::shared_ptr<cyber::Writer<ADCTrajectory>> planning_writer_;
  std::shared_ptr<cyber::Writer<routing::RoutingRequest>> rerouting_writer_;
  std::mutex mutex_;
  perception::TrafficLightDetection traffic_light_;
  routing::RoutingResponse routing_;
  planning::PadMessage pad_msg_;
  relative_map::MapMsg relative_map_;

  LocalView local_view_;

  std::unique_ptr<PlanningBase> planning_base_;  //具体的planning实例指针，实现了OnLane和NviPlanning

  PlanningConfig config_;   //planning的配置
```



### 主要函数

```
public:
  bool Init() override;

  bool Proc(const std::shared_ptr<prediction::PredictionObstacles>&
                prediction_obstacles,
            const std::shared_ptr<canbus::Chassis>& chassis,
            const std::shared_ptr<localization::LocalizationEstimate>&
                localization_estimate) override;

 private:
  void CheckRerouting();
  bool CheckInput();
```

#### Init函数
Init原型：
```
bool PlanningComponent::Init()
```
1. 初始化planning_base_:根据参数实例化具体的Planning
```
if (FLAGS_use_navigation_mode) {
    planning_base_ = std::make_unique<NaviPlanning>();
  } else {
    planning_base_ = std::make_unique<OnLanePlanning>();
  }
```
2. 加载planning config

```
ACHECK(apollo::cyber::common::GetProtoFromFile(FLAGS_planning_config_file,
                                                 &config_))
      << "failed to load planning config file " << FLAGS_planning_config_file;
  planning_base_->Init(config_);
```
3. 初始化reader和writer

```
  routing_reader_ = node_->CreateReader<RoutingResponse>(
      FLAGS_routing_response_topic,
      [this](const std::shared_ptr<RoutingResponse>& routing) {
        AINFO << "Received routing data: run routing callback."
              << routing->header().DebugString();
        std::lock_guard<std::mutex> lock(mutex_);
        routing_.CopyFrom(*routing);
      });
  traffic_light_reader_ = node_->CreateReader<TrafficLightDetection>(
      FLAGS_traffic_light_detection_topic,
      [this](const std::shared_ptr<TrafficLightDetection>& traffic_light) {
        ADEBUG << "Received traffic light data: run traffic light callback.";
        std::lock_guard<std::mutex> lock(mutex_);
        traffic_light_.CopyFrom(*traffic_light);
      });

  pad_msg_reader_ = node_->CreateReader<PadMessage>(
      FLAGS_planning_pad_topic,
      [this](const std::shared_ptr<PadMessage>& pad_msg) {
        ADEBUG << "Received pad data: run pad callback.";
        std::lock_guard<std::mutex> lock(mutex_);
        pad_msg_.CopyFrom(*pad_msg);
      });

  if (FLAGS_use_navigation_mode) {
    relative_map_reader_ = node_->CreateReader<MapMsg>(
        FLAGS_relative_map_topic,
        [this](const std::shared_ptr<MapMsg>& map_message) {
          ADEBUG << "Received relative map data: run relative map callback.";
          std::lock_guard<std::mutex> lock(mutex_);
          relative_map_.CopyFrom(*map_message);
        });
  }
  planning_writer_ =
      node_->CreateWriter<ADCTrajectory>(FLAGS_planning_trajectory_topic);

  rerouting_writer_ =
      node_->CreateWriter<RoutingRequest>(FLAGS_routing_request_topic);
```











