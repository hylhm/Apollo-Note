## Apollo曲线
curve1D类：

![](https://gitee.com/hylhm/Apollo-Note/raw/master/planning/img/curve1d.PNG)

### 曲线基类 Curve1D
虚基类，没有实现，所有的
```cpp
class Curve1d {
 public:
  Curve1d() = default;
  virtual ~Curve1d() = default;
  virtual double Evaluate(const std::uint32_t order,
                          const double param) const = 0;
  virtual double ParamLength() const = 0;
  virtual std::string ToString() const = 0;
};
```
### 多项式曲线 
```cpp
class PolynomialCurve1d : public Curve1d {
 public:
  PolynomialCurve1d() = default;
  virtual ~PolynomialCurve1d() = default;

  virtual double Coef(const size_t order) const = 0;
  virtual size_t Order() const = 0;

 protected:
  double param_ = 0.0;
};
```
### 三次多项式 
```cpp

  /**
   * x0 is the value when f(x = 0);
   * dx0 is the value when f'(x = 0);
   * ddx0 is the value when f''(x = 0);
   * f(x = param) = x1
   */
  // 构造函数
  CubicPolynomialCurve1d(const double x0, const double dx0, const double ddx0,
                         const double x1, const double param);
  // 私有成员
  std::array<double, 4> coef_ = {{0.0, 0.0, 0.0, 0.0}};
  std::array<double, 3> start_condition_ = {{0.0, 0.0, 0.0}};
  double end_condition_ = 0.0;
```

### 四次多项式
1D quartic polynomial curve: (x0, dx0, ddx0) -- [0, param] --> (dx1, ddx1)。
指定了起点的0阶导，1阶导，2阶导。参数范围和终点的1阶导，二阶导。可以用于巡航场景。
```cpp
  QuarticPolynomialCurve1d() = default; 
  
  QuarticPolynomialCurve1d(const std::array<double, 3>& start,
                           const std::array<double, 2>& end,
                           const double param);

  QuarticPolynomialCurve1d(const double x0, const double dx0, const double ddx0,
                           const double dx1, const double ddx1,
                           const double param);
  // 私有成员 
  std::array<double, 5> coef_ = {{0.0, 0.0, 0.0, 0.0, 0.0}};
  std::array<double, 3> start_condition_ = {{0.0, 0.0, 0.0}};
  std::array<double, 2> end_condition_ = {{0.0, 0.0}};

  // 主要public函数
    double Evaluate(const std::uint32_t order, const double p) const override;

  /**
   * Interface with refine quartic polynomial by meets end first order
   * and start second order boundary condition:
   * @param  x0    init point x location
   * @param  dx0   init point derivative
   * @param  ddx0  init point second order derivative
   * @param  x1    end point x location
   * @param  dx1   end point derivative
   * @param  param parameter length
   * @return       self
   */
  // 起点0、1、2阶状态,终点0阶、1阶导和参数范围。(S,V)
  QuarticPolynomialCurve1d& FitWithEndPointFirstOrder(
      const double x0, const double dx0, const double ddx0, const double x1,
      const double dx1, const double param);

  /**
   * Interface with refine quartic polynomial by meets end point second order
   * and start point first order boundary condition
   */
   // 起点0、1、2阶状态,终点1阶、2阶导和参数范围。(v,a)
  QuarticPolynomialCurve1d& FitWithEndPointSecondOrder(
      const double x0, const double dx0, const double x1, const double dx1,
      const double ddx1, const double param);

  /*
   * Integrated from cubic curve with init value
   */
  // 三次曲线积分
  QuarticPolynomialCurve1d& IntegratedFromCubicCurve(
      const PolynomialCurve1d& other, const double init_value);

  /*
   * Derived from quintic curve
   */
  // 五次曲线微分
  QuarticPolynomialCurve1d& DerivedFromQuinticCurve(
      const PolynomialCurve1d& other);

  double ParamLength() const override { return param_; }
```
### 五次多项式
1D quintic polynomial curve:
(x0, dx0, ddx0) -- [0, param] --> (x1, dx1, ddx1)
主要针对起点、终点0阶、1阶、2阶导数的曲线，包含6个系数参数。 
```cpp
  QuinticPolynomialCurve1d(const std::array<double, 3>& start,
                           const std::array<double, 3>& end,
                           const double param);

  QuinticPolynomialCurve1d(const double x0, const double dx0, const double ddx0,
                           const double x1, const double dx1, const double ddx1,
                           const double param);

  QuinticPolynomialCurve1d(const QuinticPolynomialCurve1d& other);
  double Evaluate(const std::uint32_t order, const double p) const override;
  std::array<double, 6> coef_{{0.0, 0.0, 0.0, 0.0, 0.0, 0.0}};
  std::array<double, 3> start_condition_{{0.0, 0.0, 0.0}};
  std::array<double, 3> end_condition_{{0.0, 0.0, 0.0}};
```

### 五次螺旋线
 Describe a quintic spiral path
 Map (theta0, kappa0, dkappa0) ----- delta_s -----> (theta1, kappa1, dkappa1)
以s为参数，起点$(\theta_0,kappa_0,dkappa_0)$ 、终点$(\theta_1,kappa_1,dkappa_1)$
构造函数
```cpp
  QuinticSpiralPath(const std::array<double, 3>& start,
                    const std::array<double, 3>& end, const double delta_s);  // 实际会调用下面的构造函数。

  QuinticSpiralPath(const double theta0, const double kappa0,
                    const double dkappa0, const double theta1,
                    const double kappa1, const double dkappa1,
                    const double delta_s);
```
该曲线的0阶导、1阶导、2阶导分别表示theta、kappa、dkappa关于s的函数，笛卡尔坐标可以由积分(高斯-勒让德)计算：
```cpp
//积分求x坐标  
template <size_t N>
  double ComputeCartesianDeviationX(const double s) const {
    auto cos_theta = [this](const double s) {
      const auto a = Evaluate(0, s);
      return std::cos(a);
    };
    return common::math::IntegrateByGaussLegendre<N>(cos_theta, 0.0, s);
  }
//积分求x坐标 
  template <size_t N>
  double ComputeCartesianDeviationY(const double s) const {
    auto sin_theta = [this](const double s) {
      const auto a = Evaluate(0, s);
      return std::sin(a);
    };
    return common::math::IntegrateByGaussLegendre<N>(sin_theta, 0.0, s);
  }
```
上述计算中:
$$ 
x= \int_{a}^{b} \cos{\theta} ds = \int_{a}^{b} \cos{f(s)} ds\\ 
y= \int_{a}^{b} \sin{\theta} ds = \int_{a}^{b} \sin{f(s)} ds\\ 
$$

### 分段五次螺旋线
```cpp
PiecewiseQuinticSpiralPath是由多段五次螺旋线拼接而成。

class PiecewiseQuinticSpiralPath : public Curve1d {
 public:
  PiecewiseQuinticSpiralPath(const double theta, const double kappa,
                             const double dkappa);

  virtual ~PiecewiseQuinticSpiralPath() = default;

  void Append(const double theta, const double kappa, const double dkappa,
              const double delta_s);

  double Evaluate(const std::uint32_t order, const double param) const override;

  double DeriveKappaS(const double s) const;

  double ParamLength() const override;

  std::string ToString() const override { return "PiecewiseQuinticSpiralPath"; }

 private:
  size_t LocatePiece(const double param) const;

  std::vector<QuinticSpiralPath> pieces_;

  std::vector<double> accumulated_s_;

  double last_theta_ = 0.0;

  double last_kappa_ = 0.0;

  double last_dkappa_ = 0.0;
};
```
总结：Apollo 里的一维曲线主要有多项式(3次、4次、5次)和螺旋线(也是另一种形式表示的多项式)









