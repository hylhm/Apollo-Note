## Path
### PathPoint
包含x,y,z,theta,s,kappa,dkappa,dx,dy,lane_id
区别于轨迹的地方在于:没有时间维度(t,v)
### DiscretizedPath
包含一组PathPoint,并提供了Evaluate方法(二分查找和线性插值)以及EvaluateReverse(针对倒车的s为负)

### FrenetFramePoint
包含s,l,dl,ddl

### FrenetFramePath
包含一组FrenetFramePath，并提供了EvaluateByS（二分查找和线性插值）的方法。

### PathData
PathData包含DiscretizedPath、FrenetFramePath、参考线指针
历史path信息：std::list<std::pair<DiscretizedPath, FrenetFramePath>> path_data_history_;
std::vector<std::tuple<double, PathPointType, double>>path_point_decision_guide_; //s,类型、最近障碍物距离
其中类型有：
```cpp
enum class PathPointType {
    IN_LANE,
    OUT_ON_FORWARD_LANE,
    OUT_ON_REVERSE_LANE,
    OFF_ROAD,
    UNKNOWN,
  };
  ```

## RefrenceLine相关
### RefrencePoint
RefrencePoint 继承自hdmap::MapPathPoint，另外加了kappa，dkappa
hdmap::MapPathPoint继承自Vector2D包含如下内容：
1. x
2. y
3. heading 
4. std::vector<LaneWaypoint> lane_waypoints_;
LaneWaypoint包含s、l、LaneInfo指针。
所以RefrencePoint的主要信息有：x,y,heading,kappa,dkappa
RefrencePoint的函数有：
1. 移除重复点
2. 转换到PathPoint(给定s作为参数)

### RefrenceLine
主要成员如下：
```cpp
  std::vector<SpeedLimit> speed_limit_;
  std::vector<ReferencePoint> reference_points_;
  hdmap::Path map_path_;
  uint32_t priority_ = 0;
```
主要函数包括参考线的拼接以及转换到frenet坐标。

### RefrenceLineProvider
为planning提供平滑的参考线。
从构造函数可以看出支持Hdmap和relative_map两种模式。
同时支持三种方式的参考线smoother:qp_spline,spiral,discrete_points。
不同的smoother对应不同的参数配置文件。
```cpp
ReferenceLineProvider::ReferenceLineProvider(
    const hdmap::HDMap *base_map,
    const std::shared_ptr<relative_map::MapMsg> &relative_map) {
  if (!FLAGS_use_navigation_mode) {
    pnc_map_ = std::make_unique<hdmap::PncMap>(base_map); //使用高精地图
    relative_map_ = nullptr;
  } else {
    pnc_map_ = nullptr;
    relative_map_ = relative_map; //使用relative_map 
  }
  // 根据配置文件生成
  ACHECK(cyber::common::GetProtoFromFile(FLAGS_smoother_config_filename,
                                         &smoother_config_))
      << "Failed to load smoother config file "
      << FLAGS_smoother_config_filename;
  if (smoother_config_.has_qp_spline()) {
    smoother_.reset(new QpSplineReferenceLineSmoother(smoother_config_));
  } else if (smoother_config_.has_spiral()) {
    smoother_.reset(new SpiralReferenceLineSmoother(smoother_config_));
  } else if (smoother_config_.has_discrete_points()) {
    smoother_.reset(new DiscretePointsReferenceLineSmoother(smoother_config_));
  } else {
    ACHECK(false) << "unknown smoother config "
                  << smoother_config_.DebugString();
  }
  is_initialized_ = true;
}
```
discrete_points_smoother有两种具体实现(planning/math中):
1. CosTheta
2. FemPosDevitation

更新route信息：
```cpp
bool ReferenceLineProvider::UpdateRoutingResponse(
    const routing::RoutingResponse &routing) {
  std::lock_guard<std::mutex> routing_lock(routing_mutex_);
  routing_ = routing;
  has_routing_ = true;
  return true;
}
```
获取后续路径点（只在高精地图模式使用）：

```cpp
std::vector<routing::LaneWaypoint>
ReferenceLineProvider::FutureRouteWaypoints() {
  if (!FLAGS_use_navigation_mode) {
    std::lock_guard<std::mutex> lock(pnc_map_mutex_);
    return pnc_map_->FutureRouteWaypoints();
  }

  // return an empty routing::LaneWaypoint vector in Navigation mode.
  return std::vector<routing::LaneWaypoint>();
}
```
根据Route信息更新参考线
```cpp
void ReferenceLineProvider::UpdateReferenceLine(
    const std::list<ReferenceLine> &reference_lines,
    const std::list<hdmap::RouteSegments> &route_segments)
```