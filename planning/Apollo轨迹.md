## Apollo轨迹 
Apollo中的轨迹(planning/common/trajectory1d),有如下的几种类型：
1. Stand still轨迹
2. 常减速度轨迹
3. 常jerk轨迹
4. 分段加速度轨迹
5. 分段jerk轨迹 
6. 分段轨迹  

### Stand still
Stand still很简单就是s不变，v,a,jerk全为0 

### 常减速度轨迹
给定初始S、初始v、和减速度(-a),速度减为0的轨迹。满足如下运动公式：
$$
t_{end} = v/a;  \\
s = v^2/2a;
$$
给定时间t(注意判断时间的范围)可以求得S、v、a、j

### 常jerk轨迹
给定初始s0、v0、a0和jerk值，以及时间长度的轨迹。
常jerk满足如下公式：

$$
 s = s_0+v_0 t+\frac{a_0 t^2}{2} + \frac{j t^3}{6}
$$

### 分段加速度轨迹 
给定初始的s和v,每一段的恒定加速度a和时间t

### 分段jerk轨迹 
给定初始的s和v,a,每一段的恒定加速度jerk和时间t

### 分段轨迹 
具体的每一段轨迹类型是Curve1D类型，每一段调用自己的Evaluate函数。


## 轨迹检查 
Apollo中的轨迹检查相关代码在planning/constraint_checker 
### 轨迹检查  
1. 纵向轨迹检查 
   对于轨迹上的每个时间点(时间分辨率默认0.1)检查速度、加速度、jerk的边界(非负检查)
2. 横向轨迹检查
   检查每个时间点，注意横向轨迹是基于纵向轨迹的。S-T  ,L-S 
```cpp
bool ConstraintChecker1d::IsValidLongitudinalTrajectory(
    const Curve1d& lon_trajectory) {
  double t = 0.0;
  while (t < lon_trajectory.ParamLength()) {
    double v = lon_trajectory.Evaluate(1, t);  // evaluate_v
    if (!fuzzy_within(v, FLAGS_speed_lower_bound, FLAGS_speed_upper_bound)) {
      return false;
    }

    double a = lon_trajectory.Evaluate(2, t);  // evaluate_a
    if (!fuzzy_within(a, FLAGS_longitudinal_acceleration_lower_bound,
                      FLAGS_longitudinal_acceleration_upper_bound)) {
      return false;
    }

    double j = lon_trajectory.Evaluate(3, t);
    if (!fuzzy_within(j, FLAGS_longitudinal_jerk_lower_bound,
                      FLAGS_longitudinal_jerk_upper_bound)) {
      return false;
    }
    t += FLAGS_trajectory_time_resolution;
  }
  return true;
}

bool ConstraintChecker1d::IsValidLateralTrajectory(
    const Curve1d& lat_trajectory, const Curve1d& lon_trajectory) {
  double t = 0.0;
  while (t < lon_trajectory.ParamLength()) {
    double s = lon_trajectory.Evaluate(0, t);
    double dd_ds = lat_trajectory.Evaluate(1, s);
    double ds_dt = lon_trajectory.Evaluate(1, t);

    double d2d_ds2 = lat_trajectory.Evaluate(2, s); // 横向加速度
    double d2s_dt2 = lon_trajectory.Evaluate(2, t); // 纵向加速度

    double a = 0.0;
    if (s < lat_trajectory.ParamLength()) {
      a = d2d_ds2 * ds_dt * ds_dt + dd_ds * d2s_dt2;
    }

    if (!fuzzy_within(a, -FLAGS_lateral_acceleration_bound,
                      FLAGS_lateral_acceleration_bound)) {
      return false;
    }

    // this is not accurate, just an approximation...
    double j = 0.0;
    if (s < lat_trajectory.ParamLength()) {
      j = lat_trajectory.Evaluate(3, s) * lon_trajectory.Evaluate(3, t);
    }

    if (!fuzzy_within(j, -FLAGS_lateral_jerk_bound, FLAGS_lateral_jerk_bound)) {
      return false;
    }
    t += FLAGS_trajectory_time_resolution;
  }
  return true;
}
```
纵向检查速度、加速度、jerk，横向检查加速度和jerk。另外还有曲率检查
### 碰撞检查
1. SL坐标系下后方是否有车辆。
2. 自车是否在车道内 
3. InCollision 检查每个障碍物是否和车辆碰撞。

```cpp
CollisionChecker::CollisionChecker(
    const std::vector<const Obstacle*>& obstacles, const double ego_vehicle_s,
    const double ego_vehicle_d,
    const std::vector<PathPoint>& discretized_reference_line,
    const ReferenceLineInfo* ptr_reference_line_info,
    const std::shared_ptr<PathTimeGraph>& ptr_path_time_graph) {
  ptr_reference_line_info_ = ptr_reference_line_info;
  ptr_path_time_graph_ = ptr_path_time_graph;
  BuildPredictedEnvironment(obstacles, ego_vehicle_s, ego_vehicle_d,
                            discretized_reference_line);
}
```




